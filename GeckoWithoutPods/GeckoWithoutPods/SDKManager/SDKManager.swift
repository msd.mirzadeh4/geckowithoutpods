//
//  SDKManager.swift
//  GeckoFramework
//
//  Created by Mojtaba Mirzadeh on 8/8/1399 AP.
//  Copyright © 1399 Hamrahkish. All rights reserved.
//

import Foundation
import UIKit

public class SDKManager {
    
    public init(){}
        
    public func startViewController(licenceTxt: String) -> UIViewController {
        
        let getBundle = decodCardPan(base64EncodedPan: licenceTxt)
        
        let index = (getBundle.range(of: "_")?.lowerBound)
        let beforeEqualsTo = String(getBundle.prefix(upTo: index!))
        
        if beforeEqualsTo == "com.tosan" || beforeEqualsTo == "com.gssint" {
            let s = UIStoryboard(name: "Vas", bundle: Bundle(for: SignatureCertificateFirstVC.self))
            let vc = s.instantiateViewController(withIdentifier: "Cartable") as! SignatureCertificateFirstVC
            return vc
        } else {
            let s = UIStoryboard(name: "ShowingErr", bundle: Bundle(for: ShowingErrVC.self))
            let vc = s.instantiateViewController(withIdentifier: "ShowingErr") as! ShowingErrVC
            return vc
        }
        
    }
    
    //LicenceTxtTest
    public func decodCardPan(base64EncodedPan: String) -> String {
        let key128 = arrayFrom(hexString: "2b7e151628aed2a6abf7158809cf4f3c")
        let dataPan = Data(base64Encoded: base64EncodedPan)
        let recived = Array(dataPan!)
        let cryptor = Cryptor(operation: .decrypt, algorithm: .aes, options: [.ECBMode], keyBuffer: key128, keyByteCount: key128.count, ivBuffer: "", ivByteCount: 1)
        let cipherText = cryptor.update(byteArray: recived)?.final()
        let decryptedString = cipherText!.reduce("") {$0 + String(UnicodeScalar($1))}
        let decryptPan = decryptedString.replacingOccurrences(of: "\0", with: "", options: NSString.CompareOptions.literal, range: nil)
        return decryptPan
    }
    
}
