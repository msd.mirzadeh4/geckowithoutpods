//
//  Webservice.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 5/22/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import Foundation
import Alamofire
//import SwiftyJSON
//import IDZSwiftCommonCrypto

var getFaceIDLicenceText = ""

class Webservic{
    
    static let sheared: Webservic = Webservic()
    
    let token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjYW1lbCBzZXJ2ZXIiLCJzdWIiOiJpbml0aWFsIHRva2VuIiwiYXVkIjoidXNlcnMiLCJpYXQiOjE2MDY3MTk0NzMuNDYzNzQ5NiwiZXhwIjoxNjM4MjU1NDczLCJvcyI6IkkiLCJ2ZXJzaW9uIjoiMi4wLjAiLCJvcmdhbml6YXRpb24iOiJEQVkifQ.5ZEFHKXE-oyppizcKbFK8XiYOACws2PbJlbDAlKxrW0"
    
    //let url = "https://ap.bmi.ir/60app/"
    //    let url = "http://192.168.1.70:81/camel/" //Hadi
    //    let url = "https://eid.hamrahkish.com/" //Kamran
    let url = "https://otp.bdi24.com/camel/organizations/day/"
    
    
    func contactValidation(mobileNumber: String/*, nationalCode: String*/, completionHandler: @escaping (ResponseContactValidation?, Error?) -> ()){
        
        //create parameters
        let body : [String:Any] = ["phoneNumber": mobileNumber]
        xprint(JSON(body))

        
        let header: HTTPHeaders = ["Content-Type": "application/json", "authorization":self.token]
        xprint(JSON(header))
        xprint(header)
        
        let contactValidationURL = url + "v1/send-otp"
        xprint(contactValidationURL)
        
        AF.request(contactValidationURL, method: .post, parameters:body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { responseObject in
                switch responseObject.result {
                case .success(let value):
                    print("----WS: ContactValidationReq Success-----")
                    let response = JSON(value)
                    
                    xprint(responseObject.response?.statusCode)
                    xprint(response)
                    
                    let responseContactValidation = ResponseContactValidation(statusCode: (responseObject.response?.statusCode)!, message: response["message"].stringValue)
                    
                    if responseObject.response?.statusCode != 200{
                        xprint(responseContactValidation.getMessage())
                    }
                    
                    completionHandler(responseContactValidation,nil)
                case .failure(let error):
                    print("----WS: ContactValidationReq Failed-----")
                    completionHandler(nil,error)
                    print(error)
                }
        }
    }
    
    func postInquiry(mobileNumber: String, nationalCode: String, activationCode: String, completionHandler: @escaping (ResponsePostInquiry?, Error?) -> ()){
        
        let uuid = UUID().uuidString
        
        //create parameters
        let body : [String:Any] = ["phoneNumber": mobileNumber, "udId":uuid, "nationalCode": nationalCode, "activationCode": activationCode]
        xprint(JSON(body))
        
        let header : HTTPHeaders = ["Content-Type": "application/json", "authorization": self.token]
        xprint(JSON(header))
        
        let postInquiryURL = url + "v1/users/self/inquiries/validation"
        xprint(postInquiryURL)
        
        AF.request(postInquiryURL, method: .post, parameters:body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { responseObject in
                switch responseObject.result {
                case .success(let value):
                    xprint("----WS: postInquiry Success-----")
                    let response = JSON(value)
                    xprint(response)
                    
                    let responsePostInquiry = ResponsePostInquiry()
                    
                    if responseObject.response?.statusCode == 200 {
                        
                        responsePostInquiry.setAccessToken(accessToken:  response["accessToken"].stringValue)
                        responsePostInquiry.setRefreshToken(refreshToken: response["refreshToken"].stringValue)
                        responsePostInquiry.setStatusCode(statusCode: responseObject.response!.statusCode)
                        responsePostInquiry.setMessage(message: response["message"].stringValue)
                        
                        UserDefaults.standard.set(response["accessToken"].stringValue, forKey: "accessToken")//Bool
                        UserDefaults.standard.set(response["refreshToken"].stringValue, forKey: "refreshToken")
                    } else {
                        
                        responsePostInquiry.setStatusCode(statusCode: responseObject.response!.statusCode)
                        responsePostInquiry.setMessage(message: response["message"].stringValue)
                        xprint(responseObject.response!.statusCode)
                    }
                    
                    completionHandler(responsePostInquiry,nil)
                case .failure(let error):
                    xprint("----WS: postInquiry Failed-----")
                    completionHandler(nil,error)
                    xprint(error)
                }
        }
    }
    
    func individualAndImage(serialNumber: String, birthDate: String, completionHandler: @escaping (ResponseIndividualAndImage?, Error?) -> ()){
        
        //create parameters
        let body : [String:Any] = ["serialNumber": serialNumber, "birthDate": birthDate]
        xprint(JSON(body))
        
        let header : HTTPHeaders = ["Content-Type": "application/json", "authorization":   "Bearer " + UserDefaults.standard.string(forKey: "accessToken")!]
        xprint(JSON(header))
        
        let individualAndImageURL = url + "v1/users/self/inquiries/individual-and-image"
        xprint(individualAndImageURL)
        
        AF.request(individualAndImageURL, method: .post, parameters:body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { responseObject in
                switch responseObject.result {
                case .success(let value):
                    xprint("----WS: individualAndImage Success-----")
                    let response = JSON(value)
                    xprint(response)
                    
                    let responseIndividualAndImage = ResponseIndividualAndImage()
                    
                    let responseLifeState = LifeState(title: response["lifeState"]["title"].stringValue, code: response["lifeState"]["code"].stringValue)
                    let responseGenre = GenderUser(title:response["gender"]["title"].stringValue, code: response["gender"]["code"].stringValue)
                    responseIndividualAndImage.setStatusCode(statusCode: responseObject.response!.statusCode)
                    responseIndividualAndImage.setMessage(message: response["message"].stringValue)
                    responseIndividualAndImage.setPersonImage(personImage: response["personImage"].stringValue)
                    responseIndividualAndImage.setCertificateSeries(certificateSeries: response["certificateSeries"].stringValue)
                    responseIndividualAndImage.setLastName(lastName: response["lastName"].stringValue)
                    responseIndividualAndImage.setNationalCode(nationalCode: response["nationalCode"].stringValue)
                    responseIndividualAndImage.setFatherName(fatherName: response["fatherName"].stringValue)
                    responseIndividualAndImage.setBirthDate(birthDate: response["birthDate"].stringValue)
                    responseIndividualAndImage.setCertificateNumber(certificateNumber: response["certificateNumber"].stringValue)
                    responseIndividualAndImage.setLifeState(lifeState: responseLifeState)
                    responseIndividualAndImage.setFirstName(firstName: response["firstName"].stringValue)
                    responseIndividualAndImage.setSertificateSerial(certificateSerial: response["certificateSerial"].stringValue)
                    responseIndividualAndImage.setDeathDate(deathDate: response["deatDate"].stringValue)
                    responseIndividualAndImage.setGender(gender: responseGenre)
                    responseIndividualAndImage.setPostalCode(postalCode: response["postalCode"].stringValue)
                    responseIndividualAndImage.setCityCode(cityCode: response["cityCode"].intValue)
                    responseIndividualAndImage.setStateCode(stateCode: response["stateCode"].intValue)
                    responseIndividualAndImage.setStatus(status: response["status"].stringValue)
                    responseIndividualAndImage.setEnglishFirstName(englishFirstName: response["englishFirstName"].stringValue)
                    responseIndividualAndImage.setEnglishLastName(englishLastName: response["englishLastName"].stringValue)
                    responseIndividualAndImage.setAdress(address: response["address"].stringValue)
                    
                    if responseObject.response?.statusCode != 200 {
                        responseIndividualAndImage.setStatusCode(statusCode: responseObject.response!.statusCode)
                        responseIndividualAndImage.setMessage(message: response["message"].stringValue)
                    }
                    
                    completionHandler(responseIndividualAndImage,nil)
                case .failure(let error):
                    xprint("----WS: individualAndImage Failed-----")
                    completionHandler(nil,error)
                    xprint(error)
                    
                }
        }
    }
    
    func getLicenceText(completionHandler: @escaping (ResponseGetLicenceText?, Error?) -> ()){
        
        //create parameters
        let body : [String:Any] = [:]
        xprint(JSON(body))
        
        //create parameters
        let header : HTTPHeaders = ["Content-Type": "application/json", "authorization": "Bearer " + UserDefaults.standard.string(forKey: "accessToken")!]
        xprint(header)
        
        let getLicenceTextURL = url + "v1/users/self/faces/app-key"
        xprint(getLicenceTextURL)
        
        AF.request(getLicenceTextURL, method: .get, parameters:nil, encoding: JSONEncoding.default, headers: header)
            .responseJSON { responseObject in
                switch responseObject.result {
                case .success(let value):
                    xprint("----WS: getLicenceText Success-----")
                    let response = JSON(value)
                    
                    let responseGetLicenceText = ResponseGetLicenceText(statusCode: response["status_code"].intValue, message: response["message"].stringValue)
                    
                    let decryptLicenceText = self.decodLicenceTxt(base64EncodedPan: response["licenseText"].stringValue)
                    getFaceIDLicenceText = decryptLicenceText
                    
                    xprint(getFaceIDLicenceText)
                    
                    //                    # ZoOm mobile SDK license
                    //                    appId      = com.lynx-global.*,com.gss.*,com.sadadpsp.*
                    //                    expiryDate = 2020-10-10
                    //                    key        = 0030450220630585a16975461ec8848d88ef2c390504d68c5da2d6926805c28d7a6266cb2c022100c170931d5368ecbfcfc124f444c541548b84f7c053225e519d02dd2829fa2b6f
                    //                    """
                    
                    
                    
//                    //decryptLicenceText
//                    let sentence2 = decryptLicenceText
//                    let sentence2Lines = sentence2.lines
//
//                    if let range = sentence2Lines[2].range(of: "=") {
//                        let licence = sentence2Lines[2][range.upperBound...]
//                        faceIDLicenceText = "\(licence)"
//                    } else {
//                        xprint("Licence Err")
//                    }
                    
                    completionHandler(responseGetLicenceText,nil)
                case .failure(let error):
                    xprint("----WS: getLicenceText Failed-----")
                    completionHandler(nil,error)
                    xprint(error)
                }
        }
    }
    
    
    
    func decodLicenceTxt(base64EncodedPan: String) -> String {
        
        let key128 = "qtpD!-8zJGj2zM5m"
        let dataPan = Data(base64Encoded: base64EncodedPan)
        let recived = Array(dataPan!)
        let cryptor = Cryptor(operation: .decrypt, algorithm: .aes, options: [.ECBMode], keyBuffer: key128, keyByteCount: key128.count, ivBuffer: "", ivByteCount: 1) //ivByteCount jadid ezafe shod
        let cipherText = cryptor.update(byteArray: recived)?.final()
        let decryptedString = cipherText!.reduce("") {$0 + String(UnicodeScalar($1))}
        let decryptPan = decryptedString.replacingOccurrences(of: "\0", with: "", options: NSString.CompareOptions.literal, range: nil)
        return decryptPan
        
    }
    
    func sessionToken(completionHandler: @escaping (ResponseSessionToken?, Error?) -> ()) {
        
        //create parameters
        let header : HTTPHeaders = ["Content-Type": "application/json", "authorization": "Bearer " + UserDefaults.standard.string(forKey: "accessToken")!]
        xprint(header)
        
        let sessionTokenURL = url + "v1/users/self/faces/session-token"
        xprint(sessionTokenURL)
        
        AF.request(sessionTokenURL, method: .get, parameters:nil, encoding: JSONEncoding.default, headers: header)
            .responseJSON { responseObject in
                switch responseObject.result {
                case .success(let value):
                    xprint("----WS: sessionToken Success-----")
                    let response = JSON(value)
                    xprint(response)
                    
                    let responseSessionToken = ResponseSessionToken(statusCode: response["status_code"].intValue, message: response["message"].stringValue, sessionToken: response["sessionToken"].stringValue)
                    
                    completionHandler(responseSessionToken,nil)
                case .failure(let error):
                    xprint("----WS: sessionToken Failed-----")
                    completionHandler(nil,error)
                    xprint(error)
                }
        }
        
    }
    
    func faceMatch(sourceFaceMap: String, auditTrailImage: String, lowQualityAuditTrailImage: String, sessionId: String, completionHandler: @escaping (ResponseFaceMatch?, Error?) -> ()){
        
        //create parameters
        let header : HTTPHeaders = ["Content-Type": "application/json", "authorization": "Bearer " + UserDefaults.standard.string(forKey: "accessToken")!]
        xprint(header)
        
        //create body
        let body : [String:Any] = ["sourceFaceMap": sourceFaceMap, "auditTrailImage": auditTrailImage, "lowQualityAuditTrailImage": lowQualityAuditTrailImage, "sessionId": sessionId]
        xprint(JSON(body))
        
        let faceMatchURL = url + "v1/users/self/faces/match-3d-2d"
        xprint(faceMatchURL)
        
        AF.request(faceMatchURL, method: .post, parameters:body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { responseObject in
                switch responseObject.result {
                case .success(let value):
                    xprint("----WS: faceMatch Success-----")
                    let response = JSON(value)
                    xprint(response)
                    
                    let responseFaceId = ResponseFaceMatch(statusCode: response["status_code"].intValue, message: response["message"].stringValue)
                    
                    completionHandler(responseFaceId,nil)
                case .failure(let error):
                    xprint("----WS: faceMatch Failed-----")
                    completionHandler(nil,error)
                    xprint(error)
                }
        }
    }
    
    func sendSignature(signature: String, completionHandler: @escaping (ResponseSignature?, Error?) -> ()){
        
        //create parameters
        let header : HTTPHeaders = ["Content-Type": "application/json", "authorization":   "Bearer " + UserDefaults.standard.string(forKey: "accessToken")!]
        xprint(header)
        
        //create body
        let body : [String:Any] = ["signature": signature]
        xprint(JSON(body))
        
        let sendSignatureURL = url + "v1/users/self/signature"
        xprint(sendSignatureURL)
        
        AF.request(sendSignatureURL, method: .patch, parameters:body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { responseObject in
                switch responseObject.result {
                case .success(let value):
                    xprint("----WS: sendSignature Success-----")
                    let response = JSON(value)
                    xprint(response)
                    
                    let responseSinature = ResponseSignature(statusCode: response["status_code"].intValue, message: response["message"].stringValue)
                    
                    completionHandler(responseSinature,nil)
                case .failure(let error):
                    xprint("----WS: sendSignature Failed-----")
                    completionHandler(nil,error)
                    xprint(error)
                }
        }
    }
    
    func postSignCSR(englishFirstName: String, englishLastName: String, address: String, cityCode: Int, email: String, jobTitle: String, homeNumber: String, postalCode: String, stateCode: Int, csr: String, completionHandler: @escaping (ResponsePostSignCSR?, Error?) -> ()){
        
        //create parameters
        let header : HTTPHeaders = ["Content-Type": "application/json", "authorization":   "Bearer " + UserDefaults.standard.string(forKey: "accessToken")!]
        xprint(header)
        
        //create body
        let body : [String:Any] = ["englishFirstName": englishFirstName, "englishLastName":englishLastName , "address":address , "cityCode":cityCode , "email": email, "jobTitle": jobTitle, "homeNumber": homeNumber, "postalCode":postalCode , "stateCode": stateCode, "csr":csr]
        xprint(JSON(body))
        
        let postSignCSRURL = url + "v1/users/self/csr/sign"
        xprint(postSignCSRURL)
        
        let manager = Session.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request(postSignCSRURL, method: .post, parameters:body, encoding: JSONEncoding.default, headers: header)
            .responseJSON { responseObject in
                switch responseObject.result {
                case .success(let value):
                    xprint("----WS: postSignCSR Success-----")
                    let response = JSON(value)
                    xprint(response)
                    
                    let responsePostSignCSR = ResponsePostSignCSR()
                    
                    if responseObject.response?.statusCode == 200 {
                        
                        responsePostSignCSR.setStatusCode(statusCode: responseObject.response!.statusCode)
                        responsePostSignCSR.setMessage(message: response["message"].stringValue)
                        
                        responsePostSignCSR.setTraceID(traceID: response["traceId"].stringValue)
                        responsePostSignCSR.setServerCSR(serverCSR: response["csr"].stringValue)
                        
                        UserDefaults.standard.set(response["traceId"].stringValue, forKey: "traceId")
                        
                    } else {
                        responsePostSignCSR.setStatusCode(statusCode: responseObject.response!.statusCode)
                        responsePostSignCSR.setMessage(message: response["message"].stringValue)
                    }
                    
                    completionHandler(responsePostSignCSR,nil)
                case .failure(let error):
                    xprint("----WS: postSignCSR Failed-----")
                    completionHandler(nil,error)
                    xprint(error)
                }
        }
    }
    
    func getState(completionHandler: @escaping ([ResponseStateCode]?,Error?) -> ()) {
        
        var states = [ResponseStateCode]()
        
        let header : HTTPHeaders = ["Content-Type": "application/json", "authorization":   "Bearer " + UserDefaults.standard.string(forKey: "accessToken")!]
        xprint(header)
        
        let getStateURL = url + "v1/states"
        xprint(getStateURL)
        
        AF.request(getStateURL, method: .get, headers: header).responseJSON { response in
            switch response.result {
            case .success(let value):
                print("----WS: getState OK-----")
                
                if let httpCode = response.response?.statusCode{
                    if httpCode == 200{
                        xprint("getState 200")
                        
                        let json = JSON(value)
                        for item in json.arrayValue {
                            let state = ResponseStateCode(stateCode: item["code"].intValue, stateName: item["name"].stringValue)/*(stateId: item["state_id"].intValue,stateName: item["state_name"].stringValue)*/
                            states.append(state)
                        }
                        
                        completionHandler(states,nil)
                    }
                }else{
                    xprint("getState 6258")
                    completionHandler(nil,nil)
                }
                
            case .failure(let error):
                print("----WS: getState Failed-----")
                completionHandler(nil,error)
            }
        }
    }
    
    func getCity(urlFromState: Int, completionHandler: @escaping ([ResponseCityCode]?,Error?) -> ()) {
        var cities = [ResponseCityCode]()
        
        let header : HTTPHeaders = ["Content-Type": "application/json", "authorization":   "Bearer " + UserDefaults.standard.string(forKey: "accessToken")!]
        xprint(header)
        
        let getCityURL = url + "v1/states/\(urlFromState)/cities"
        xprint(getCityURL)
        
        AF.request(getCityURL, method: .get, headers: header).responseJSON { response in
            switch response.result {
            case .success(let value):
                print("----WS: getCity OK-----")
                
                if let httpCode = response.response?.statusCode{
                    if httpCode == 200{
                        
                        let json = JSON(value)
                        for item in json.arrayValue {
                            let city = ResponseCityCode(cityCode: item["code"].intValue, cityName: item["name"].stringValue, stateCode: item["stateCode"].intValue)
                            cities.append(city)
                        }
                        
                        completionHandler(cities,nil)
                    }
                }else{
                    completionHandler(nil,nil)
                }
                
            case .failure(let error):
                print("----WS: getCity Failed-----")
                completionHandler(nil,error)
            }
        }
    }
    
}
