//
//  ResponsePostSignCSR.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 6/11/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import Foundation

class ResponsePostSignCSR {
    
    var statusCode: Int!
    var message: String!
    var serverCSR: String!
    var traceID: String!
    
    //MARK- Setter -
    func setStatusCode(statusCode: Int) {
        self.statusCode = statusCode
    }
    
    func setMessage(message: String) {
        self.message = message
    }
    
    func setServerCSR(serverCSR: String) {
        self.serverCSR = serverCSR
    }
    
    func setTraceID(traceID: String) {
        self.traceID = traceID
    }
    
    //MARK- Getter -
    func getServerCSR()-> String {
        return self.serverCSR
    }

    func getTraceID()-> String {
        return self.traceID
    }

    func getStatusCode()-> Int {
        return self.statusCode
    }
    
    func getMessage()-> String {
        return self.message
    }
    
}
