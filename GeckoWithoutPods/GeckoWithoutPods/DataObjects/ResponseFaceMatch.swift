//
//  ResponseFaceMatch.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 6/11/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import Foundation

class ResponseFaceMatch{

    var statusCode: Int!
    var message: String!
    
    init(statusCode: Int, message: String){
        self.statusCode = statusCode
        self.message = message
    }
    
    //MARK - Gtter -
    func getStatusCode()-> Int{
        return self.statusCode
    }
    func getMessage()-> String{
        return self.message
    }
}
