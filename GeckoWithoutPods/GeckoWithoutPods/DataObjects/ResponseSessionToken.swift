//
//  ResponseSessionToken.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 6/22/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import Foundation

class ResponseSessionToken {

    var statusCode: Int!
    var message: String!
    var sessionToken: String!
    
    init(statusCode: Int, message: String, sessionToken: String){
        self.statusCode = statusCode
        self.message = message
        self.sessionToken = sessionToken
    }
    
    //MARK - Gtter -
    func getStatusCode()-> Int {
        return self.statusCode
    }
    func getMessage()-> String {
        return self.message
    }
    func getSessionToken()-> String {
        return self.sessionToken
    }
}
