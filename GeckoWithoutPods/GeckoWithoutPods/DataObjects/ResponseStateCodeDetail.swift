//
//  ResponseStateCodeDetail.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 6/24/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import Foundation

class ResponseStateCodeDetail {
    
    var id: Int!
    var createdBy: String!
    var createDate: String!
    var enName: String!
    var name: String!
    var code: Int!
    
    init(id: Int, name: String, code: Int){
        self.id = id
        self.name = name
        self.code = code
        
        self.createdBy = nil
        self.createDate = nil
        self.enName = nil
    }
    
    //MARK- Getter -
    func getId()-> Int{
        return self.id
    }
    
    func getName()-> String{
        return self.name
    }
    
    func getCode()-> Int{
        return self.code
    }
}
