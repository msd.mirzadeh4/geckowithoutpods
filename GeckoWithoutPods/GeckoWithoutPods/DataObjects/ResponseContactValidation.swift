//
//  ResponseContactValidation.swift
//  eik
//
//  Created by Erfan Saadatmand on 7/7/20.
//  Copyright © 2020 Erfan Saadatmand. All rights reserved.
//

import Foundation

class ResponseContactValidation{

    var statusCode: Int!
    var message: String!
        
    init(statusCode: Int, message: String){
        self.statusCode = statusCode
        self.message = message
    }
    
    //Setter
    func setStatusCode(statusCode:Int){
       self.statusCode = statusCode
    }
    
    //MARK - Gtter -
    func getStatusCode()-> Int{
        return self.statusCode
    }
    func getMessage()-> String{
        return self.message
    }
    
}

