//
//  EnglishNumber.swift
//  Cheetah
//
//  Created by Erfan Saadatmand on 1/6/18.
//  Copyright © 2018 GSSInt. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func english() -> String {
        var myInt = String(self)
        //arabic
        myInt = myInt.replacingOccurrences(of: "٠", with: "0")
        myInt = myInt.replacingOccurrences(of: "١", with: "1")
        myInt = myInt.replacingOccurrences(of: "٢", with: "2")
        myInt = myInt.replacingOccurrences(of: "٣", with: "3")
        myInt = myInt.replacingOccurrences(of: "٤", with: "4")
        myInt = myInt.replacingOccurrences(of: "٥", with: "5")
        myInt = myInt.replacingOccurrences(of: "٦", with: "6")
        myInt = myInt.replacingOccurrences(of: "٧", with: "7")
        myInt = myInt.replacingOccurrences(of: "٨", with: "8")
        myInt = myInt.replacingOccurrences(of: "٩", with: "9")
        //persian
        myInt = myInt.replacingOccurrences(of: "۰", with: "0")
        myInt = myInt.replacingOccurrences(of: "۱", with: "1")
        myInt = myInt.replacingOccurrences(of: "۲", with: "2")
        myInt = myInt.replacingOccurrences(of: "۳", with: "3")
        myInt = myInt.replacingOccurrences(of: "۴", with: "4")
        myInt = myInt.replacingOccurrences(of: "۵", with: "5")
        myInt = myInt.replacingOccurrences(of: "۶", with: "6")
        myInt = myInt.replacingOccurrences(of: "۷", with: "7")
        myInt = myInt.replacingOccurrences(of: "۸", with: "8")
        myInt = myInt.replacingOccurrences(of: "۹", with: "9")
        return myInt
    }
}
