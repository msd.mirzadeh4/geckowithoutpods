//
//  Keychain.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 9/3/1399 AP.
//  Copyright © 1399 AP GSSInt. All rights reserved.
//

import Foundation

class KeyChain {
    
    class func save(key: String, data: NSData) {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data ] as [String : Any] as [String : Any] as [String : Any]

        SecItemDelete(query as CFDictionary)

        let status: OSStatus = SecItemAdd(query as CFDictionary, nil)

    }

    class func load(key: String) -> NSData? {
        let query = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue,
            kSecMatchLimit as String  : kSecMatchLimitOne ] as [String : Any]

//        var dataTypeRef :Unmanaged<AnyObject>?
        var dataTypeRef:AnyObject? = nil
        
        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)
//        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, nil)

        if status == noErr {
            return (dataTypeRef! as! NSData)
        } else {
            return nil
        }


    }

    class func stringToNSDATA(string : String)->NSData
    {
        let _Data = (string as NSString).data(using: String.Encoding.utf8.rawValue)
        return _Data! as NSData

    }


    class func NSDATAtoString(data: NSData)->String
    {
        var returned_string : String = NSString(data: data as Data, encoding: String.Encoding.utf8.rawValue)! as String
        return returned_string
    }

}
