//
//  PersianNumber.swift
//  Cheetah
//
//  Created by Reza Hamedi on 10/14/1396 AP.
//  Copyright © 1396 GSSInt. All rights reserved.
//

import UIKit

public extension String {
    
    func persian() -> String {
    
        
        var myInt = String(self)
        
        myInt = myInt.replacingOccurrences(of: "0", with: "۰")
        myInt = myInt.replacingOccurrences(of: "1", with: "۱")
        myInt = myInt.replacingOccurrences(of: "2", with: "۲")
        myInt = myInt.replacingOccurrences(of: "3", with: "۳")
        myInt = myInt.replacingOccurrences(of: "4", with: "۴")
        myInt = myInt.replacingOccurrences(of: "5", with: "۵")
        myInt = myInt.replacingOccurrences(of: "6", with: "۶")
        myInt = myInt.replacingOccurrences(of: "7", with: "۷")
        myInt = myInt.replacingOccurrences(of: "8", with: "۸")
        myInt = myInt.replacingOccurrences(of: "9", with: "۹")
        myInt = myInt.replacingOccurrences(of: "٠", with: "۰")
        myInt = myInt.replacingOccurrences(of: "٢", with: "۲")
        myInt = myInt.replacingOccurrences(of: "٣", with: "۳")
        myInt = myInt.replacingOccurrences(of: "٤", with: "۴")
        myInt = myInt.replacingOccurrences(of: "٥", with: "۵")
        myInt = myInt.replacingOccurrences(of: "٦", with: "۶")
        myInt = myInt.replacingOccurrences(of: "٧", with: "۷")
        myInt = myInt.replacingOccurrences(of: "٨", with: "۸")
        myInt = myInt.replacingOccurrences(of: "٩", with: "۹")
        
        myInt = myInt.replacingOccurrences(of: "Saturday", with: "شنبه")
        myInt = myInt.replacingOccurrences(of: "Sunday", with: "یکشنبه")
        myInt = myInt.replacingOccurrences(of: "Monday", with: "دوشنبه")
        myInt = myInt.replacingOccurrences(of: "Tuesday", with: "سه شنبه")
        myInt = myInt.replacingOccurrences(of: "Wednesday", with: "چهارشنبه")
        myInt = myInt.replacingOccurrences(of: "Thursday", with: "پنج شنبه")
        myInt = myInt.replacingOccurrences(of: "Friday", with: "جمعه")
        
        myInt = myInt.replacingOccurrences(of: "Farvardin", with: "فروردین")
        myInt = myInt.replacingOccurrences(of: "Ordibehesht", with: "اردیبهشت")
        myInt = myInt.replacingOccurrences(of: "Khordad", with: "خرداد")
        myInt = myInt.replacingOccurrences(of: "Tir", with: "تیر")
        myInt = myInt.replacingOccurrences(of: "Mordad", with: "مرداد")
        myInt = myInt.replacingOccurrences(of: "Shahrivar", with: "شهریور")
        myInt = myInt.replacingOccurrences(of: "Mehr", with: "مهر")
        myInt = myInt.replacingOccurrences(of: "Aban", with: "آبان")
        myInt = myInt.replacingOccurrences(of: "Azar", with: "آذر")
        myInt = myInt.replacingOccurrences(of: "Dey", with: "دی")
        myInt = myInt.replacingOccurrences(of: "Bahman", with: "بهمن")
        myInt = myInt.replacingOccurrences(of: "Esfand", with: "اسفند")
        
        myInt = myInt.replacingOccurrences(of: "AP", with: "")
        
        return myInt
        
    }
}
