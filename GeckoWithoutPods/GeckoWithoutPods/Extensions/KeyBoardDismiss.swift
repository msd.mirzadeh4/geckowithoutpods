//
//  KeyBoardDismiss.swift
//  Cheetah
//
//  Created by Reza Hamedi on 10/14/1396 AP.
//  Copyright © 1396 GSSInt. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        
        //Moshkele Touch TableView
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
