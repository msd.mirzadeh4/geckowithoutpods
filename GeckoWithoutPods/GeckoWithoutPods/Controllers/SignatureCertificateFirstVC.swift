//
//  SignatureCertificateFirstVC.swift
//  Cheetah
//
//  Created by Mojtaba Mirzadeh on 5/21/1399 AP.
//  Copyright © 1399 GSSInt. All rights reserved.
//

import UIKit
//import Toast_Swift
//import NVActivityIndicatorView
import ZoomAuthentication
//import MKMagneticProgress

class SignatureCertificateFirstVC: UIViewController, NVActivityIndicatorViewable, UITextFieldDelegate, URLSessionDelegate {
    
    @IBOutlet weak var mobileTxtField: UITextField!
    @IBOutlet weak var codeMelliTxtField: UITextField!
    @IBOutlet weak var activationCode: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
//    @IBOutlet weak var magProgress: MKMagneticProgress!
//    @IBOutlet weak var headerImg: UIImageView!
    
    //Layouts
    @IBOutlet weak var codeMelliTxtToStatusLbl: NSLayoutConstraint!
    
    //Views
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var codeMelliView: UIView!
    @IBOutlet weak var activationCodeView: UIView!
    
    //Labels
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var codeMelliLabel: UILabel!
    @IBOutlet weak var activationCodeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    var responseContactValidation: ResponseContactValidation!
    var responsePostInquiry = ResponsePostInquiry()
    
    var isSuccess = false
    var requestInProgress: Bool = false
    
    var phoneNumber = ""
    
    private var garegoriBirthday: String = "1991-01-02"
    
//    let datePicker = DatePickerDialog(textColor: .black, buttonColor: .white, font: UIFont(name: "IRANSans-Bold", size: 13)! ,buttonBackgroundColor: #colorLiteral(red: 0.07843137255, green: 0.337254902, blue: 0.6588235294, alpha: 1), showCancelButton: false)
    
    let formatter = DateFormatter()
    
//    private var seconds = 60
//    private var minutes = 0
//    weak var timer : Timer?
//    private var timeLeft = 60.00
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initVC()
        
        //        self.birthDateTxtField.delegate = self
        
        self.activationCode.delegate = self
        if #available(iOS 12.0, *) {
            self.activationCode.keyboardType = .numberPad
            self.activationCode.textContentType = .oneTimeCode
        }
        
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.activationCode.resignFirstResponder()
        return true
    }
    
    func initVC() {
        
        self.mobileTxtField.attributedPlaceholder = NSAttributedString(string: "۰۹xxxxxxxxx",
        attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2789010108, green: 0.2789084315, blue: 0.2789044678, alpha: 1)])
        //.text = "۰۹" + User.getUserInstance().getPhoneNumber().persian()
        self.codeMelliTxtField.attributedPlaceholder = NSAttributedString(string: "کد ملی بدون خط تیره",
                                                                          attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2789010108, green: 0.2789084315, blue: 0.2789044678, alpha: 1)])
        self.activationCode.attributedPlaceholder = NSAttributedString(string: "کد تایید",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2789010108, green: 0.2789084315, blue: 0.2789044678, alpha: 1)])
        
//        self.headerImg.image = UIImage(named: "pic")
        self.mobileLabel.isEnabled = false
//        self.mobileTxtField.isEnabled = false
        self.codeMelliLabel.isHidden = true
        self.codeMelliView.isHidden = true
        self.activationCodeLabel.isHidden = true
        self.activationCodeView.isHidden = true
//        self.magProgress.isHidden = true
        self.statusLabel.isHidden = true
        
        // Add Image And Font And Title For NavigationBar
//        let attributes = [NSAttributedString.Key.font: UIFont(name: "IRANSans-Bold", size: 20)!]
        self.navigationController?.navigationBar.backgroundColor = UIColor(hexString: "#1456A8")
//        UINavigationBar.appearance().titleTextAttributes = attributes
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        navigationItem.backBarButtonItem = backButton
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        hideKeyboardWhenTappedAround()
    }
    
//    func setMagProgress(){
//        magProgress.setProgress(progress: 1.0, animated: true)
//        magProgress.progressShapeColor = #colorLiteral(red: 0.07843137255, green: 0.337254902, blue: 0.6588235294, alpha: 1)
//
//        magProgress.backgroundShapeColor = UIColor.lightGray
//        magProgress.titleColor = UIColor.red
//        magProgress.percentColor = UIColor.black
//        magProgress.font = UIFont(name: "IRANSans-Light", size: 10)!
//
//        magProgress.lineWidth = 5
//        magProgress.orientation = .bottom
//        magProgress.lineCap = .square
//
//        magProgress.title = ""
//        // magProgress.percentLabelFormat = "%.2f%%"
//        let s = String(format:"%02i", self.seconds).persian()
//        //        let m = String(format:"%02i", 00).persian()
//        self.magProgress.percentLabelFormat = "\(s)" //"\(m):\(s)"
//
//    }
    
//    @objc func timerAction() {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            self.timeLeft -= 1
//            self.magProgress.setProgress(progress: CGFloat(self.timeLeft), animated: true)
//            if self.minutes == 0 {
//                self.seconds -= 1
//                let s = String(format:"%02i", self.seconds).persian()
//                self.magProgress.percentLabelFormat = "\(s)"
//                if self.seconds == 0 {
//                    self.magProgress.isHidden = true
//                    self.stopTimer()
//                    self.navigationController?.popViewController(animated: true)
//                }
//            }
//        }
//    }
    
//    func stopTimer(){
//        self.timer?.invalidate()
//        self.timer = nil
//    }
    
//    func setTimer(){
//        timer?.invalidate()
//        // start the timer
//        timer = Timer.scheduledTimer(timeInterval: 1.0
//            , target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
//        timer!.tolerance = 0.2
//
//    }
    
    //CodeMelli Validator
    func isValidNationalCode(input: String) -> Bool {
        let digits = input.compactMap { Int(String($0)) }
        guard digits.count == 10 && digits.count == input.count  else {
            return false
        }
        let check = digits[9]
        let sum = digits[0 ..< 9].enumerated().reduce(0) { $0 + $1.element * (10 - $1.offset) } % 11
        return sum < 2 ? check == sum : check + sum == 11
    }
    
    //Call Webservice
    func checkContactValidation() {
        self.startAnimating()
        Webservic.sheared.contactValidation(mobileNumber: self.mobileTxtField.text!.english()){response, error in
            if response == nil {
                
                self.view.makeToast("مشکلی در اتصال به شبکه پیش آمده لطفا بعدا امتحان کنید.", duration: 2, position: .center)
                self.stopAnimating()
                
            } else {
                
                if(response?.getStatusCode() == 200){
                    self.stopAnimating()
                    self.responseContactValidation = response
                    
//                    self.headerImg.image = UIImage(named: "Group 4858")
                    self.mobileLabel.isHidden = true
                    self.mobileView.isHidden = true
                    self.codeMelliLabel.isHidden = false
                    self.codeMelliView.isHidden = false
                    self.activationCodeLabel.isHidden = false
                    self.activationCodeView.isHidden = false
//                    self.magProgress.isHidden = false
                    self.statusLabel.isHidden = false
                    self.codeMelliTxtToStatusLbl.constant = 0
//                    self.statusLabel.text = "شماره ارسالی به شماره" + " " + "۰۹" + User.getUserInstance().getPhoneNumber().persian() + " " + "و کد ملی خود را وارد کنید"
                    
//                    //Timer
//                    self.seconds = 60
//                    self.minutes = 0
//                    self.timeLeft = 60.00
//                    self.setMagProgress()
//                    self.setTimer()
                    
                }else{
                    self.stopAnimating()
                    self.view.makeToast(response?.getMessage(), duration: 2, position: .center)
                }
            }
            
        }
        
    }
    
    //Call Webservice
    func postInquiry() {
        self.startAnimating()
        
        Webservic.sheared.postInquiry(mobileNumber: (self.mobileTxtField.text?.english())!, nationalCode: (self.codeMelliTxtField.text?.english())!, activationCode: (self.activationCode.text?.english())!) { (response, err) in
            if response == nil {
                self.stopAnimating()
                self.view.makeToast("مشکلی در اتصال به شبکه پیش آمده لطفا بعدا امتحان کنید.", duration: 2, position: .center)
            } else if response?.getStatusCode() != 200 {
                self.stopAnimating()
                self.view.makeToast(response?.getMessage(), duration: 2, position: .center)
                
            } else {
                
                self.stopAnimating()
                
                xprint(response)
                
                xprint(response!)
                
                self.responsePostInquiry = response!
                
                self.mobileLabel.isHidden = true
                self.mobileView.isHidden = true
//                self.stopTimer()
                
                self.performSegue(withIdentifier: "GoToCardmelli", sender: self)
                
            }
            
        }
        
    }
    
    func startAnimating(){
        let size = CGSize(width: 70, height: 70)
        let font = UIFont(name: "IRANSans-Light", size: 15)
        startAnimating(size, message: "در حال بارگذاری...", messageFont: font, type: NVActivityIndicatorType.ballClipRotateMultiple, color: #colorLiteral(red: 0.07843137255, green: 0.337254902, blue: 0.6588235294, alpha: 1),
                       displayTimeThreshold: 4, minimumDisplayTime: 2, textColor: UIColor.white)
    }
    
    //Actions
    @IBAction func texfieldEditingChangged(_ sender: UITextField) {
        if sender ==  self.mobileTxtField {
            self.mobileTxtField.text = sender.text?.persian()
        }else if sender == self.codeMelliTxtField {
            self.codeMelliTxtField.text = sender.text?.persian()
        } else {
            self.activationCode.text = sender.text?.persian()
        }
    }
    
    @IBAction func confirmBtnPressed(_ sender: Any) {
        if self.mobileView.isHidden == false {
            if mobileTxtField.text!.count > 10 {
                
                xprint(self.mobileTxtField.text!.english())
                
                if UserDefaults.standard.bool(forKey: self.mobileTxtField.text!.english()) {
                    self.view.makeToast("شما قبلا ثبت نام کرده اید", duration: 5, position: .center)
                } else {
                    checkContactValidation()
                }

            }else{
                self.view.makeToast("شماره تلفن نادرست است.", duration: 2, position: .center)
            }
        } else if self.codeMelliView.isHidden == false && self.activationCodeView.isHidden == false {
            if self.activationCode.text!.count == 6 {
                if isValidNationalCode(input: codeMelliTxtField.text!.english()) {
                    self.postInquiry()
                } else {
                    self.view.makeToast("کد ملی نادرست است.", duration: 2, position: .center)
                }
            } else {
                self.view.makeToast("کد تایید را با دقت وارد کنید.", duration: 2, position: .center)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "GoToCardmelli") {
            let nationalCardVC = segue.destination as? NationalCardVC
            nationalCardVC?.responseContactValidation = self.responseContactValidation
            nationalCardVC?.responsePostInquiry = self.responsePostInquiry
            nationalCardVC!.phoneNumber = self.mobileTxtField.text!
        }
        
    }
    
}

