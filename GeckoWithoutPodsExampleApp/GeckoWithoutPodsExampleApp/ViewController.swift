//
//  ViewController.swift
//  GeckoWithoutPodsExampleApp
//
//  Created by Mojtaba Mirzadeh on 9/29/1399 AP.
//  Copyright © 1399 AP Hamrahkish. All rights reserved.
//

import UIKit
import Foundation
import GeckoWithoutPods

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func callFrameworkPressed(_ sender: Any) {
        let manager = SDKManager()
        let vc = manager.startViewController(licenceTxt: "")
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

